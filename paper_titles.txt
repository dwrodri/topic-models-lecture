 Evolving openMP for evolving architectures
 AlleAlle bounded relational model finding with unbounded data
 Hardware accelerated SAT solversA survey
 A Sound for a Sound Mitigating Acoustic Side Channel Attacks on Password Keystrokes with Active Sounds
 Kodkod A Relational Model Finder
 PipeProof Automated Memory Consistency Proofs for Microarchitectural Specifications
 CheckMate Automated Synthesis of Hardware Exploits and Security Litmus Tests
 Security Verification via Automatic HardwareAware Exploit Synthesis The CheckMate Approach
 A Fast LockFree Internal Binary Search Tree
 Efﬁcient Transparent and Comprehensive Runtime Code Manipulation
 NVBit A Dynamic Binary Instrumentation Framework for NVIDIA GPUs
 Dissecting the NVIDIA Volta GPU Architecture via Microbenchmarking
 PCGRL Procedural Content Generation via Reinforcement Learning
 SALSA scalable and low synchronization NUMAaware algorithm for producerconsumer pools
 ConTExT LeakageFree Transient Execution
 DDR5 design challenges
 Unveiling the potential of Graph Neural Networks for network modeling and optimization in SDN
 Jellyfish Networking Data Centers Randomly
 On the problem of expanding hypercubebased systems
 Convolutional Neural Networks as a Model of the Visual System Past Present and Future
 Differential Power Analysis
 Speculator a tool to analyze speculative execution attacks and mitigations
 Lets Not Speculate Discovering and Analyzing Speculative Execution Attacks
 MODELING EFFECTS OF SPECULATIVE INSTRUCTION EXECUTION IN A FUNCTIONAL CACHE SIMULATOR
 Spectre Returns Speculation Attacks using the Return Stack Buffer
 An evaluation of speculative instruction execution on simultaneous multithreaded processors
 NetSpectre Read Arbitrary Memory over Network
 SMoTherSpectre exploiting speculative execution through port contention
 Speculative Taint Tracking STT A Comprehensive Protection for Speculatively Accessed Data
 How the spectre and meltdown hacks really worked
 The gem5 simulator
 PTLsim A Cycle Accurate Full System x8664 Microarchitectural Simulator
 ZombieLoad CrossPrivilegeBoundary Data Sampling
 A Systematic Evaluation of Transient Execution Attacks and Defenses
 Notation as a Tool of Thought
 NDA Preventing Speculative Execution Attacks at Their Source
 NeuroVectorizer EndtoEnd Vectorization with Deep Reinforcement Learning
 SafeSpec Banishing the Spectre of a Meltdown with LeakageFree Speculation
 Deep Learning for Symbolic Mathematics
 A General and Adaptive Robust Loss Function
 Financial Time Series Forecasting with Deep Learning  A Systematic Literature Review 20052019
 Wayback Machine
 Improving Directmapped Cache Performance by the Addition of a Small Fullyassociative Cache and Prefetch Buffers
 CEASER Mitigating ConflictBased Cache Attacks via EncryptedAddress and Remapping
 Completely Fair Scheduler and its tuning
 CACHE MISSING FOR FUN AND PROFIT
 Runahead execution an alternative to very large instruction windows for outoforder processors
 The Optimum Pipeline Depth for a Microprocessor
 Speculative execution via address prediction and data prefetching
 Alternative Implementations of Twolevel Adaptive Branch Prediction
 IEEEACM International Symposium on Microarchitecture Micro29
 Experiment flows and microbenchmarks for reverse engineering of branch predictor structures
 Available Instruction Level Parallelism for Superscalar and Superpipelined Machines
 Dynamic branch prediction with perceptrons
 Meltdown Reading Kernel Memory from User Space
 Spectre Attacks Exploiting Speculative Execution
 When a Patch is Not Enough  HardFails SoftwareExploitable Hardware Bugs
 ArMOR defending against memory consistency model mismatches in heterogeneous architectures
 Data Orchestration in Deep Learning Accelerators
 Cache Replacement Policies
 RealityCheck Bringing Modularity Hierarchy and Abstraction to Automated Microarchitectural Memory Consistency Verification
 PipeCheck Specifying and Verifying Microarchitectural Enforcement of Memory Consistency Models
 TriCheck Memory Model Verification at the Trisection of Software Hardware and ISA
 COATCheck Verifying Memory Ordering at the HardwareOS Interface
 BlackParrot An Agile OpenSource RISCV Multicore for Accelerator SoCs
 CheckMate Automated Synthesis of Hardware Exploits and Security Litmus Tests
 Graph2Seq Graph to Sequence Learning with Attentionbased Neural Networks
 Sparse SequencetoSequence Models
 Parallel WaveNet Fast HighFidelity Speech Synthesis
 WaveNet A Generative Model for Raw Audio
 WaveNet A Generative Model for Raw Audio
 Onsets and Frames DualObjective Piano Transcription
 Transistency Models Memory Ordering at the HardwareOS Interface
 CROSSTALK Speculative Data Leaks Across Cores Are Real
 Speculative Probing Hacking Blind in the Spectre Era
 ERASER A Benchmark to Evaluate Rationalized NLP Models
 Enriching Word Embeddings Using Knowledge Graph for Semantic Tagging in Conversational Dialog Systems
 Researchpaper recommender systems a literature survey
 An introduction to latent semantic analysis
 TransForm Formally Specifying Transistency Models and Synthesizing Enhanced Litmus Tests
 Array programming with NumPy
 Twoshot Spatiallyvarying BRDF and Shape Estimation
 FlexiTaint A programmable accelerator for dynamic taint propagation
 ArchitectureIndependent Dynamic Information Flow Tracking
 LVI Hijacking Transient Execution through Microarchitectural Load Value Injection
 The Evolution of TransientExecution Attacks
 Evolution of Defenses against TransientExecution Attacks
 Complete information flow tracking from the gates up
 A small and adaptive coprocessor for information flow tracking in ARM SoCs
 The Normal Distribution A derivation from basic principles
 Inceptionv4 InceptionResNet and the Impact of Residual Connections on Learning
 Selflabelling via simultaneous clustering and representation learning
 Latent Cross Making Use of Context in Recurrent Recommender Systems
 Deep Neural Networks for YouTube Recommendations
 Deep Learning for Recommender Systems
 Probabilistic Matrix Factorization
 Are we really making much progress A worrying analysis of recent neural recommendation approaches
 Matrix Factorization Techniques for Recommender Systems
 Construction of the Literature Graph in Semantic Scholar
 Recommendation systems Principles methods and evaluation
 Research paper recommender system evaluation a quantitative literature survey
 Take A Way Exploring the Security Implications of AMDs Cache Way Predictors
 Towards Accurate Multiperson Pose Estimation in the Wild
 An Ondevice Deep Neural Network for Face Detection
 Stabilized realtime face tracking via a learned dynamic rigidity prior
 Depth Prediction Without the Sensors Leveraging Structure for Unsupervised Learning from Monocular Videos
 Face recognition in unconstrained videos with matched background similarity
 DensePose Dense Human Pose Estimation In The Wild
 MobileNets Efficient Convolutional Neural Networks for Mobile Vision Applications
 Fast and globally convergent pose estimation from video images
 Towards Fast Accurate and Stable 3D Dense Face Alignment
 Predicting Citation Counts Using Text and Graph Mining
 Partially Labeled Topic Models for Interpretable Text Mining
 A Comprehensive Survey on Graph Neural Networks
 Speculative Interference Attacks Breaking Invisible Speculation Schemes
 Data Oblivious ISA Extensions for Side ChannelResistant and High Performance Computing
 Spectrum  Classifying  Replicating and Mitigating Spectre Attacks on a Speculating RISCV Microarchitecture
 MeltdownPrime and SpectrePrime AutomaticallySynthesized Attacks Exploiting InvalidationBased Coherence Protocols
 Property specific information flow analysis for hardware security verification
 NDA Preventing Speculative Execution Attacks at Their Source
 ContentBased Citation Recommendation
 HighPrecision Extraction of Emerging Concepts from Scientific Literature
 SciREX A Challenge Dataset for DocumentLevel Information Extraction
 SciBERT A Pretrained Language Model for Scientific Text
 Rigorous engineering for hardware security Formal modelling and proof in the CHERI design and implementation process  IEEE Conference Publication
 Visualizing and Measuring the Geometry of BERT
 Online Learning for Latent Dirichlet Allocation
 Stochastic Variational Inference
 MOLIERE Automatic Biomedical Hypothesis Generation System
 Finding Complex Biological Relationships in Recent PubMed Articles Using BioLDA
 Supervised Topic Models
 Endtoend automated exploit generation for validating the security of processor designs
 Attention is All you Need
 The Efficient Application of Automatic Differentiation for Computing Gradients in Financial Applications
 InvisiSpec Making Speculative Execution Invisible in the Cache Hierarchy
 A pretty but not greedy printer functional pearl
 Ray A Distributed Framework for Emerging AI Applications
 Mastering Atari Go Chess and Shogi by Planning with a Learned Model
 Why Do Nigerian Scammers Say They are From Nigeria
 Understanding the GPU Microarchitecture to Achieve BareMetal Performance Tuning
 Data Oblivious ISA Extensions for Side ChannelResistant and High Performance Computing
 MLIR A Compiler Infrastructure for the End of Moores Law
 KASLR Break It Fix It Repeat
 Specifying Verifying and Translating Between Memory Consistency Models
 Highdimensional dynamics of generalization error in neural networks
 BERT Pretraining of Deep Bidirectional Transformers for Language Understanding
 An Image is Worth 16x16 Words Transformers for Image Recognition at Scale
 Variational Inference A Review for Statisticians
 SentenceBERT Sentence Embeddings using Siamese BERTNetworks
 Bayesian Neural Networks
 Mixing Dirichlet Topic Models and Word Embeddings to Make lda2vec
 Top2Vec Distributed Representations of Topics
 HIBERT Document Level Pretraining of Hierarchical Bidirectional Transformers for Document Summarization
 XLNet Generalized Autoregressive Pretraining for Language Understanding
 Seq2seq Translation Model for Sequential Recommendation
 Latent Dirichlet Allocation
 Distributed Representations of Words and Phrases and their Compositionality
 Path confidence based lookahead prefetching
 Practical Bayesian Optimization of Machine Learning Algorithms
 Google Vizier A Service for BlackBox Optimization
