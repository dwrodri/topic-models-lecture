import numpy as np
from numpy.core.fromnumeric import nonzero
from numpy.linalg.linalg import norm
import sys
import matplotlib.pyplot as plt
import seaborn as sns


def get_ngrams(doc: str, gram_size: int = 2) -> list[str]:
    """Get n-grams from string, default 2 (bigrams)"""
    return [doc[i : i + gram_size] for i in range(len(doc) - gram_size + 1)]


if __name__ == "__main__":
    # Load the corpus into memory as a list of strings
    with open(sys.argv[1], "r") as filepointer:
        corpus = filepointer.readlines()  # type: list(str)

    # clean the corpus
    for i in range(len(corpus)):
        corpus[i] = "".join(
            [
                char  # type: str
                for char in corpus[i].lower().strip()
                if char.isalnum() or char.isspace()
            ]
        )

    # Map words in the corpus to a series of numbers
    token_mapping = {
        text: token
        for token, text in enumerate(
            set([word for document in corpus for word in document.split()])
        )
    }

    # Create doc-term matrix
    tf_matrix = np.zeros((len(corpus), len(token_mapping)), dtype=np.float_)
    for doc_idx in range(len(corpus)):
        wordlist = corpus[doc_idx].split()
        for term_idx in range(len(wordlist)):
            col_idx = token_mapping[wordlist[term_idx]]
            tf_matrix[doc_idx, col_idx] += 1

    # Create TF-IDF matrix
    idf_vector = len(corpus) / np.count_nonzero(tf_matrix, axis=0)
    tf_idf_matrix = tf_matrix * idf_vector
    tf_idf_matrix /= np.linalg.norm(tf_idf_matrix, ord=2, axis=1)[:, None]

    doc_topic_matrix, topic_space, term_topic_matrix = np.linalg.svd(tf_idf_matrix)
    # get topics
    topic_combinations = term_topic_matrix[:, :2].transpose()
    topic_vectors = np.argsort(topic_combinations, axis=1)[:, ::-1][:, :10]
    print(topic_vectors)
    reverse_token_mapping = list(token_mapping.keys())
    for topic_idx in range(len(topic_vectors)):
        decoded_topic = ", ".join(
            [reverse_token_mapping[token] for token in topic_vectors[topic_idx]]
        )
        print(f"Topic #{topic_idx}: {decoded_topic}")
    # viz_documents
    sns.set(font_scale=1.5)
    for i in range(len(corpus)):
        plt.plot(doc_topic_matrix[0, i], doc_topic_matrix[1, i], "k.")
    for i in range(5):
        plt.plot(
            doc_topic_matrix[0, i],
            doc_topic_matrix[1, i],
            "o",
            linewidth=50,
            label=corpus[-i],
        )
    plt.legend()
    plt.show()
