import numpy as np
from numpy.core.fromnumeric import nonzero
from numpy.linalg.linalg import norm
import sys


def get_ngrams(doc: str, gram_size: int = 2) -> list[str]:
    """Get n-grams from string, default 2 (bigrams)"""
    return [doc[i : i + gram_size] for i in range(len(doc) - gram_size + 1)]


if __name__ == "__main__":
    # Load the corpus into memory as a list of strings
    with open(sys.argv[1], "r") as filepointer:
        corpus = filepointer.readlines()  # type: list(str)

    # clean the corpus
    for i in range(len(corpus)):
        corpus[i] = "".join(
            [
                char  # type: str
                for char in corpus[i].lower().strip()
                if char.isalnum() or char.isspace()
            ]
        )

    # Map words in the corpus to a series of numbers
    token_mapping = {
        text: token
        for token, text in enumerate(
            set([word for document in corpus for word in get_ngrams(document)])
        )
    }

    # Create doc-term matrix
    tf_matrix = np.zeros((len(corpus), len(token_mapping)), dtype=np.float_)
    for doc_idx in range(len(corpus)):
        wordlist = get_ngrams(corpus[doc_idx])
        for term_idx in range(len(wordlist)):
            col_idx = token_mapping[wordlist[term_idx]]
            tf_matrix[doc_idx, col_idx] += 1

    # Create TF-IDF matrix
    idf_vector = len(corpus) / np.count_nonzero(tf_matrix, axis=0)
    tf_idf_matrix = tf_matrix * idf_vector

    # Perform TF-IDF Search
    userin = get_ngrams(input("Enter a search query: ").lower())
    query = np.zeros(len(token_mapping), dtype=np.float_)
    for ngram in userin:
        try:
            query[token_mapping[ngram]] += 1
        except KeyError:
            continue
    # Scale everything by its L2 norm
    query /= np.linalg.norm(query)
    tf_idf_matrix /= np.linalg.norm(tf_idf_matrix, ord=2, axis=1)[:, None]
    # Calculate Similarity and print top 10 results
    key = tf_idf_matrix @ query  # The @ a dot product
    print("-" * 10 + "RESULTS" + "-" * 10)
    for result_idx in np.argsort(key)[::-1][:10]:
        print(corpus[result_idx])
